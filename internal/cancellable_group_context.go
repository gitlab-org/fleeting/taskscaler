package internal

import (
	"context"
	"sync"
)

// CancellableGroupContext provides a context factory of contexts that can be
// all cancelled together.
type CancellableGroupContext struct {
	mu         sync.Mutex
	once       sync.Once
	done       chan struct{}
	doneReason error
}

// NewCancellableGroupContext returns a new cancellable group context.
func NewCancellableGroupContext() *CancellableGroupContext {
	return &CancellableGroupContext{
		done: make(chan struct{}),
	}
}

// Cancel all active contexts with the provided error cause.
func (cgc *CancellableGroupContext) Cancel(err error) {
	cgc.once.Do(func() {
		cgc.mu.Lock()
		cgc.doneReason = err
		cgc.mu.Unlock()

		close(cgc.done)
	})
}

// WithContext returns a new context that can be cancelled via an outside
// caller calling Cancel on the group.
func (cgc *CancellableGroupContext) WithContext(parent context.Context) (context.Context, context.CancelFunc) {
	ctx, cancel := context.WithCancelCause(parent)

	if ctx.Err() == nil {
		go func() {
			select {
			case <-cgc.done:
				cgc.mu.Lock()
				reason := cgc.doneReason
				cgc.mu.Unlock()

				cancel(reason)
			case <-ctx.Done():
			}
		}()
	}

	return ctx, func() { cancel(nil) }
}
