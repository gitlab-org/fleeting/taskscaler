package capacity

import (
	"fmt"
	"math"
	"strconv"
	"strings"
)

type CapacityInfo struct {
	// InstanceCount is active instances: creating, running, requested
	InstanceCount int

	// MaxInstanceCount is the maximum number of instances supported by the
	// instance group.
	MaxInstanceCount int

	// Acquired is instance capacity that has been acquired
	Acquired int

	// UnavailableCapacity is instance capacity that is unable to be used, this
	// is typically due to an instance having a max number of uses.
	// For example, an instance with a capacity of 5, but only 4 more uses,
	// effectively only has a capacity of 4.
	UnavailableCapacity int

	// Pending is pending tasks that do not yet have an acquisition
	Pending int

	// Reserved is reserved capacity that does not yet have an acquisition
	Reserved int

	// IdleCount is the desired idle capacity we want to maintain
	IdleCount int

	// ScaleFactor is used for calculating additional idle capacity based on
	// demand
	ScaleFactor float64

	// ScaleFactorLimit limits additional idle capacity
	ScaleFactorLimit int

	// CapacityPerInstance is how much capacity an instance provides
	CapacityPerInstance int
}

func (c CapacityInfo) String() string {
	return strings.Join([]string{
		fmt.Sprintf("instance_count:%d", c.InstanceCount),
		fmt.Sprintf("max_instance_count:%d", c.MaxInstanceCount),
		fmt.Sprintf("acquired:%d", c.Acquired),
		fmt.Sprintf("unavailable_capacity:%d", c.UnavailableCapacity),
		fmt.Sprintf("pending:%d", c.Pending),
		fmt.Sprintf("reserved:%d", c.Reserved),
		fmt.Sprintf("idle_count:%d", c.IdleCount),
		fmt.Sprintf("scale_factor:%s", strconv.FormatFloat(c.ScaleFactor, 'f', -1, 64)),
		fmt.Sprintf("scale_factor_limit:%d", c.ScaleFactorLimit),
		fmt.Sprintf("capacity_per_instance:%d", c.CapacityPerInstance),
	}, ",")
}

// RequiredInstance returns how many instances should be added or removed, based
// upon existing capacity and demand. It returns 0 if no change is required.
func RequiredInstances(info CapacityInfo) int {
	// If acquired + unavailable capacity is above what can be provided by the
	// current number of instances than don't scale.
	//
	// This prevents a demand runaway scenario where the demand for capacity
	// was previously satisfied by the current number of instances, but then
	// the instances were removed. In this case, we shouldn't re-create
	// capacity to satisfy this perceived demand, but instead wait for
	// acquisitions to be released or for unavailable capacity to be removed.
	if info.Acquired+info.UnavailableCapacity > info.InstanceCount*info.CapacityPerInstance {
		return 0
	}

	calcRequiredInstances := func(reserved int) int {
		// pending is pending tasks with no acquisition, if the required instances
		// would have been negative, we also add "reserved" here, to prevent
		// scaling down for reservations.
		pending := info.Pending + reserved

		// the pending capacity might be wholly satisfied by the idle count, so
		// the demand for new capacity is the larger value between the idle
		// count we wish to maintain and the pending capacity.
		demand := max(info.IdleCount, pending)

		// the desired capacity is our existing acquired capacity plus demand
		desired := info.Acquired + demand

		// use (acquired*scale factor) if it's greater than our existing desired
		// capacity
		if info.ScaleFactor > 0 {
			scale := int(math.Ceil(info.ScaleFactor * float64(info.Acquired)))
			if info.ScaleFactorLimit > 0 {
				scale = min(scale, info.ScaleFactorLimit)
			}
			desired = max(desired, scale)
		}

		// We treat unavailable capacity as capacity that will forever be "acquired"
		desired += info.UnavailableCapacity

		// required is how many instances are required in order to satisfy the
		// desired capacity it is desired divided by the capacity per instance
		required := int(math.Ceil(float64(desired) / float64(info.CapacityPerInstance)))

		// the required capacity is modified based upon how many instances are
		// already in-flight (requesting + creating) or are currently processing the
		// acquired demand.
		required -= info.InstanceCount

		// don't scale more than the maximum instance count
		if info.MaxInstanceCount > 0 && required > 0 {
			return min(required, max(0, info.MaxInstanceCount-info.InstanceCount))
		}

		return required
	}

	required := calcRequiredInstances(0)

	// Unlike pending capacity, which is the result of Acquire, we don't
	// actively desire reserved capacity. However, we also don't scale
	// down instances that could provide reserved capacity. This also
	// protects us when in preemptive mode from removing reserved capacity
	// that was promised from immediately available capacity in the event
	// of idle scale rule changes.
	if required < 0 {
		required = calcRequiredInstances(info.Reserved)
		if required >= 0 {
			return 0
		}
	}

	return required
}
