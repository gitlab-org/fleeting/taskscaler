package capacity

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func required(req int, info CapacityInfo) func(t *testing.T) {
	return func(t *testing.T) {
		t.Helper()

		n := RequiredInstances(info)
		if n != req {
			t.Errorf("expected %d, got %d", req, n)
		}
	}
}

func TestCapacityBasic(t *testing.T) {
	t.Run("10 acquired, 5 instances, 3 capacity per instance, needs -1 instances", required(-1, CapacityInfo{
		Acquired:            10,
		InstanceCount:       5,
		MaxInstanceCount:    5,
		CapacityPerInstance: 3,
	}))

	t.Run("10 acquired, 10 instances, 3 capacity per instance, needs -1 instances", required(-6, CapacityInfo{
		Acquired:            10,
		InstanceCount:       10,
		MaxInstanceCount:    5,
		CapacityPerInstance: 3,
	}))

	t.Run("1 acquired, 1 instances, 1 capacity per instance, needs 0 instances", required(0, CapacityInfo{
		Acquired:            1,
		InstanceCount:       1,
		CapacityPerInstance: 1,
	}))

	t.Run("1 pending, 1 instances, 1 capacity per instance, needs 0 instances", required(0, CapacityInfo{
		Pending:             1,
		InstanceCount:       1,
		CapacityPerInstance: 1,
	}))

	t.Run("1 pending, 0 instances, 1 capacity per instance, needs 1 instances", required(1, CapacityInfo{
		Pending:             1,
		InstanceCount:       0,
		CapacityPerInstance: 1,
	}))

	t.Run("100 acquired, 1 instances, 100 capacity per instance, needs 0 instances", required(0, CapacityInfo{
		Acquired:            100,
		InstanceCount:       1,
		CapacityPerInstance: 100,
	}))

	t.Run("100 pending, 1 instances, 100 capacity per instance, needs 0 instances", required(0, CapacityInfo{
		Pending:             100,
		InstanceCount:       1,
		CapacityPerInstance: 100,
	}))

	t.Run("100 pending, 0 instances, 100 capacity per instance, needs 1 instances", required(1, CapacityInfo{
		Pending:             100,
		InstanceCount:       0,
		CapacityPerInstance: 100,
	}))

	t.Run("2 pending, 2 acquired, 2 instances, 2 capacity per instance, needs 0 instances", required(0, CapacityInfo{
		Pending:             2,
		Acquired:            2,
		InstanceCount:       2,
		CapacityPerInstance: 2,
	}))

	t.Run("3 pending, 2 acquired, 2 instances, 2 capacity per instance, needs 1 instances", required(1, CapacityInfo{
		Pending:             3,
		Acquired:            2,
		InstanceCount:       2,
		CapacityPerInstance: 2,
	}))

	t.Run("2 pending, 1 acquired, 1 unavailable, 1 instances, 2 capacity per instance, needs 0 instances", required(0, CapacityInfo{
		Pending:             2,
		Acquired:            1,
		UnavailableCapacity: 1,
		InstanceCount:       2,
		CapacityPerInstance: 2,
	}))

	t.Run("1 pending, 1 acquired, 1 unavailable, 1 instances, 2 capacity per instance, needs 0 instances", required(0, CapacityInfo{
		Pending:             1,
		Acquired:            1,
		UnavailableCapacity: 1,
		InstanceCount:       2,
		CapacityPerInstance: 2,
	}))

	t.Run("1 pending, 1 acquired, 3 unavailable, 1 instances, 2 capacity per instance, needs 1 instances", required(1, CapacityInfo{
		Pending:             1,
		Acquired:            1,
		UnavailableCapacity: 3,
		InstanceCount:       2,
		CapacityPerInstance: 2,
	}))

	// special case where acquired+unavailable is greater than the number of instances already, so we don't scale until
	// acquisitions are released
	t.Run("1 pending, 2 acquired, 3 unavailable, 1 instances, 2 capacity per instance, needs 0 instances", required(0, CapacityInfo{
		Pending:             1,
		Acquired:            2,
		UnavailableCapacity: 3,
		InstanceCount:       2,
		CapacityPerInstance: 2,
	}))

	t.Run("1 pending, 4 unavailable, 2 instances, 2 capacity per instance, needs 1 instances", required(1, CapacityInfo{
		Pending:             1,
		Acquired:            0,
		UnavailableCapacity: 4,
		InstanceCount:       2,
		CapacityPerInstance: 2,
	}))

	t.Run("100 acquired, 101 pending, 1 instances, 100 capacity per instance, needs 2 instance", required(2, CapacityInfo{
		Acquired:            100,
		Pending:             101,
		InstanceCount:       1,
		CapacityPerInstance: 100,
	}))

	t.Run("100 acquired, 101 pending, 1 instances, 100 capacity per instance, 2 max instance count, needs 1 instance", required(1, CapacityInfo{
		Acquired:            100,
		Pending:             101,
		InstanceCount:       1,
		CapacityPerInstance: 100,
		MaxInstanceCount:    2,
	}))
}

func TestCapacity2xScaleFactor(t *testing.T) {
	t.Run("1 acquired, 1 instances, 1 capacity per instance, needs 1 instances", required(1, CapacityInfo{
		Acquired:            1,
		InstanceCount:       1,
		ScaleFactor:         2,
		CapacityPerInstance: 1,
	}))

	t.Run("100 acquired, 1 instances, 100 capacity per instance, needs 1 instances", required(1, CapacityInfo{
		Acquired:            100,
		InstanceCount:       1,
		ScaleFactor:         2,
		CapacityPerInstance: 100,
	}))

	t.Run("1 pending, 0 instances, 1 capacity per instance, needs 1 instance", required(1, CapacityInfo{
		Pending:             1,
		InstanceCount:       0,
		ScaleFactor:         2,
		CapacityPerInstance: 1,
	}))

	t.Run("100 pending, 0 instances, 100 capacity per instance, needs 1 instance", required(1, CapacityInfo{
		Pending:             100,
		InstanceCount:       0,
		ScaleFactor:         2,
		CapacityPerInstance: 100,
	}))

	t.Run("2 acquired, 2 instances, 1 capacity per instance, needs 2 instances", required(2, CapacityInfo{
		Acquired:            2,
		InstanceCount:       2,
		ScaleFactor:         2,
		CapacityPerInstance: 1,
	}))

	t.Run("100 acquired, 100 pending, 1 instances, 100 capacity per instance, needs 1 instance", required(1, CapacityInfo{
		Acquired:            100,
		Pending:             100,
		InstanceCount:       1,
		ScaleFactor:         2,
		CapacityPerInstance: 100,
	}))

	t.Run("100 acquired, 100 pending, 2 instances, 100 capacity per instance, needs 0 instance", required(0, CapacityInfo{
		Acquired:            100,
		Pending:             100,
		InstanceCount:       2,
		ScaleFactor:         2,
		CapacityPerInstance: 100,
	}))

	t.Run("100 acquired, 101 pending, 1 instances, 100 capacity per instance, 2 max instance count, needs 1 instance", required(1, CapacityInfo{
		Acquired:            100,
		Pending:             101,
		InstanceCount:       1,
		ScaleFactor:         2,
		CapacityPerInstance: 100,
		MaxInstanceCount:    2,
	}))

	t.Run("100 acquired, 200 pending, 1 instances, 100 capacity per instance, 1 max instance count, needs 0 instance", required(0, CapacityInfo{
		Acquired:            100,
		Pending:             200,
		InstanceCount:       1,
		ScaleFactor:         2,
		CapacityPerInstance: 100,
		MaxInstanceCount:    1,
	}))
}

func TestCapacityUsedCounts(t *testing.T) {
	t.Run("1 instance, 2 capacity per instance, one unavailable, nothing pending", required(0, CapacityInfo{
		InstanceCount:       1,
		Acquired:            1,
		CapacityPerInstance: 2,
		UnavailableCapacity: 1,
	}))

	t.Run("1 instance, 2 capacity per instance, one unavailable, one pending", required(1, CapacityInfo{
		InstanceCount:       1,
		Acquired:            1,
		CapacityPerInstance: 2,
		Pending:             1,
		UnavailableCapacity: 1,
	}))

	t.Run("2 instances, 2 capacity per instance, two unavailable, two pending", required(0, CapacityInfo{
		InstanceCount:       2,
		Acquired:            0,
		CapacityPerInstance: 2,
		Pending:             2,
		UnavailableCapacity: 2,
	}))

	t.Run("2 instances, 2 capacity per instance, two unavailable, two pending", required(0, CapacityInfo{
		InstanceCount:       2,
		Acquired:            0,
		CapacityPerInstance: 2,
		Pending:             2,
		UnavailableCapacity: 2,
	}))
}

func TestCapacityOnePerInstance(t *testing.T) {
	t.Run("at capacity, nothing pending", required(0, CapacityInfo{
		InstanceCount:       10,
		Acquired:            10,
		CapacityPerInstance: 1,
	}))

	t.Run("at capacity, 1 pending", required(1, CapacityInfo{
		InstanceCount:       10,
		Acquired:            10,
		CapacityPerInstance: 1,
		Pending:             1,
	}))

	t.Run("at capacity, 5 pending", required(5, CapacityInfo{
		InstanceCount:       10,
		Acquired:            10,
		CapacityPerInstance: 1,
		Pending:             5,
	}))

	t.Run("at capacity, 1 pending, 1 idle", required(1, CapacityInfo{
		InstanceCount:       10,
		Acquired:            10,
		CapacityPerInstance: 1,
		Pending:             1,
		IdleCount:           1,
	}))

	t.Run("at capacity, 1 pending, 5 idle", required(5, CapacityInfo{
		InstanceCount:       10,
		Acquired:            10,
		CapacityPerInstance: 1,
		Pending:             1,
		IdleCount:           5,
	}))

	t.Run("at capacity, 5 pending, 5 idle", required(5, CapacityInfo{
		InstanceCount:       10,
		Acquired:            10,
		CapacityPerInstance: 1,
		Pending:             5,
		IdleCount:           5,
	}))

	t.Run("at capacity, 5 pending, 1 idle", required(5, CapacityInfo{
		InstanceCount:       10,
		Acquired:            10,
		CapacityPerInstance: 1,
		Pending:             5,
		IdleCount:           1,
	}))

	t.Run("at capacity, 5 pending, 1 idle, 10 max instance count", required(0, CapacityInfo{
		InstanceCount:       10,
		Acquired:            10,
		CapacityPerInstance: 1,
		Pending:             5,
		IdleCount:           1,
		MaxInstanceCount:    10,
	}))

	t.Run("at capacity, 5 pending, 1 idle, 5 max instance count", required(0, CapacityInfo{
		InstanceCount:       10,
		Acquired:            10,
		CapacityPerInstance: 1,
		Pending:             5,
		IdleCount:           1,
		MaxInstanceCount:    5,
	}))

	t.Run("at capacity, 5 pending, 1 idle, 4 max instance count", required(0, CapacityInfo{
		InstanceCount:       10,
		Acquired:            10,
		CapacityPerInstance: 1,
		Pending:             5,
		IdleCount:           1,
		MaxInstanceCount:    4,
	}))

	t.Run("under capacity, nothing pending, 1 idle", required(0, CapacityInfo{
		InstanceCount:       10,
		Acquired:            9,
		CapacityPerInstance: 1,
		IdleCount:           1,
	}))

	t.Run("under capacity, nothing pending", required(-1, CapacityInfo{
		InstanceCount:       10,
		Acquired:            9,
		CapacityPerInstance: 1,
	}))

	t.Run("under capacity, 1 pending", required(-10, CapacityInfo{
		InstanceCount:       20,
		Acquired:            9,
		CapacityPerInstance: 1,
		Pending:             1,
	}))

	t.Run("under capacity, 5 pending, 1 idle", required(-6, CapacityInfo{
		InstanceCount:       20,
		Acquired:            9,
		CapacityPerInstance: 1,
		Pending:             5,
		IdleCount:           1,
	}))

	t.Run("under capacity, 5 pending, 1 idle, 5 max instance count", required(-6, CapacityInfo{
		InstanceCount:       20,
		Acquired:            9,
		CapacityPerInstance: 1,
		Pending:             5,
		IdleCount:           1,
		MaxInstanceCount:    5,
	}))
}

func TestCapacityTwoPerInstance(t *testing.T) {
	t.Run("at capacity, nothing pending", required(0, CapacityInfo{
		InstanceCount:       5,
		Acquired:            10,
		CapacityPerInstance: 2,
	}))

	t.Run("at capacity, 1 pending", required(1, CapacityInfo{
		InstanceCount:       5,
		Acquired:            10,
		CapacityPerInstance: 2,
		Pending:             1,
	}))

	t.Run("at capacity, 5 pending", required(3, CapacityInfo{
		InstanceCount:       5,
		Acquired:            10,
		CapacityPerInstance: 2,
		Pending:             5,
	}))

	t.Run("at capacity, 1 pending, 1 idle", required(1, CapacityInfo{
		InstanceCount:       5,
		Acquired:            10,
		CapacityPerInstance: 2,
		Pending:             1,
		IdleCount:           1,
	}))

	t.Run("at capacity, 1 pending, 5 idle", required(3, CapacityInfo{
		InstanceCount:       5,
		Acquired:            10,
		CapacityPerInstance: 2,
		Pending:             1,
		IdleCount:           5,
	}))

	t.Run("at capacity, 5 pending, 5 idle", required(3, CapacityInfo{
		InstanceCount:       5,
		Acquired:            10,
		CapacityPerInstance: 2,
		Pending:             5,
		IdleCount:           5,
	}))

	t.Run("at capacity, 5 pending, 1 idle", required(3, CapacityInfo{
		InstanceCount:       5,
		Acquired:            10,
		CapacityPerInstance: 2,
		Pending:             5,
		IdleCount:           1,
	}))

	t.Run("at capacity, 5 pending, 1 idle, 10 max instance count", required(3, CapacityInfo{
		InstanceCount:       5,
		Acquired:            10,
		CapacityPerInstance: 2,
		Pending:             5,
		IdleCount:           1,
		MaxInstanceCount:    10,
	}))

	t.Run("at capacity, 5 pending, 1 idle, 3 max instance count", required(0, CapacityInfo{
		InstanceCount:       5,
		Acquired:            10,
		CapacityPerInstance: 2,
		Pending:             5,
		IdleCount:           1,
		MaxInstanceCount:    3,
	}))

	t.Run("at capacity, 5 pending, 1 idle, 2 max instance count", required(0, CapacityInfo{
		InstanceCount:       5,
		Acquired:            10,
		CapacityPerInstance: 2,
		Pending:             5,
		IdleCount:           1,
		MaxInstanceCount:    2,
	}))

	t.Run("under capacity, nothing pending, 1 idle", required(0, CapacityInfo{
		InstanceCount:       5,
		Acquired:            9,
		CapacityPerInstance: 2,
		IdleCount:           1,
	}))

	t.Run("under capacity, nothing pending, 2 idle", required(1, CapacityInfo{
		InstanceCount:       5,
		Acquired:            9,
		CapacityPerInstance: 2,
		IdleCount:           2,
	}))

	t.Run("under capacity, nothing pending", required(-1, CapacityInfo{
		InstanceCount:       5,
		Acquired:            8,
		CapacityPerInstance: 2,
	}))

	t.Run("under capacity, 1 pending", required(-5, CapacityInfo{
		InstanceCount:       10,
		Acquired:            9,
		CapacityPerInstance: 2,
		Pending:             1,
	}))

	t.Run("under capacity, 5 pending, 1 idle", required(-3, CapacityInfo{
		InstanceCount:       10,
		Acquired:            9,
		CapacityPerInstance: 2,
		Pending:             5,
		IdleCount:           1,
	}))

	t.Run("under capacity, 5 pending, 1 idle, 10 max instance count", required(-3, CapacityInfo{
		InstanceCount:       10,
		Acquired:            9,
		CapacityPerInstance: 2,
		Pending:             5,
		IdleCount:           1,
		MaxInstanceCount:    10,
	}))
}

func TestCapacityReservations(t *testing.T) {
	t.Run("1 acquired, 1 capacity per instance, 1 reserved, needs 0 instance", required(0, CapacityInfo{
		InstanceCount:       1,
		Acquired:            1,
		CapacityPerInstance: 1,
		Reserved:            1,
	}))

	t.Run("2 instances, 0 acquired, 1 capacity per instance, 1 reserved, 1 idle, needs -1", required(-1, CapacityInfo{
		InstanceCount:       2,
		Acquired:            0,
		CapacityPerInstance: 1,
		Reserved:            1,
		IdleCount:           1,
	}))

	t.Run("2 instances, 0 acquired, 1 capacity per instance, 100 reserved, 1 idle, needs 0", required(0, CapacityInfo{
		InstanceCount:       2,
		Acquired:            0,
		CapacityPerInstance: 1,
		Reserved:            100,
		IdleCount:           1,
	}))

	t.Run("2 instances, 0 acquired, 1 capacity per instance, 1 reserved, 1 idle, 2 pending, needs 1", required(1, CapacityInfo{
		InstanceCount:       2,
		Acquired:            0,
		CapacityPerInstance: 1,
		Reserved:            1,
		IdleCount:           1,
		Pending:             3,
	}))

	t.Run("10 instances, 2 acquired, 1 capacity per instance, 5 idle, 0 reserved, needs -3", required(-3, CapacityInfo{
		InstanceCount:       10,
		Acquired:            2,
		CapacityPerInstance: 1,
		IdleCount:           5,
		Reserved:            0,
	}))

	t.Run("10 instances, 2 acquired, 1 capacity per instance, 5 idle, 2 reserved, needs -3", required(-3, CapacityInfo{
		InstanceCount:       10,
		Acquired:            2,
		CapacityPerInstance: 1,
		IdleCount:           5,
		Reserved:            2,
	}))

	t.Run("10 instances, 2 acquired, 5 capacity per instance, 5 idle, 2 reserved, needs -8", required(-8, CapacityInfo{
		InstanceCount:       10,
		Acquired:            2,
		CapacityPerInstance: 5,
		IdleCount:           5,
		Reserved:            2,
	}))

	t.Run("10 instances, 2 acquired, 5 capacity per instance, 5 idle, 5 reserved, needs -8", required(-8, CapacityInfo{
		InstanceCount:       10,
		Acquired:            2,
		CapacityPerInstance: 5,
		IdleCount:           5,
		Reserved:            5,
	}))

	t.Run("10 instances, 2 acquired, 5 capacity per instance, 5 idle, 5 reserved, 10 max instance count, needs -8", required(-8, CapacityInfo{
		InstanceCount:       10,
		Acquired:            2,
		CapacityPerInstance: 5,
		IdleCount:           5,
		Reserved:            5,
		MaxInstanceCount:    10,
	}))
}

func TestCapacityInfoString(t *testing.T) {
	assert.Equal(t,
		"instance_count:10,"+
			"max_instance_count:0,"+
			"acquired:2,"+
			"unavailable_capacity:0,"+
			"pending:0,"+
			"reserved:5,"+
			"idle_count:5,"+
			"scale_factor:1.5,"+
			"scale_factor_limit:0,"+
			"capacity_per_instance:5",
		CapacityInfo{
			InstanceCount:       10,
			Acquired:            2,
			CapacityPerInstance: 5,
			IdleCount:           5,
			Reserved:            5,
			ScaleFactor:         1.5,
		}.String(),
	)
}
