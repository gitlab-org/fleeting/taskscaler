package ratelimit

import (
	"math"
	"math/rand"
	"sync"
	"time"

	"golang.org/x/time/rate"
)

const (
	DefaultBackoffMinimum = 10 * time.Second
	DefaultBackoffMaximum = 20 * time.Minute
	DefaultBackoffFactor  = 1.2
	DefaultBackoffJitter  = 0.2
)

// Limiter is a rate limiter and has an exponential backoff implementation in
// the event of too many errors which further reduces the rate of operations.
//
// In the context of taskscaler, it's used when scaling up instances, and caps
// how many instances can be requested per second. It has the ability to burst,
// so for example, if you allow 100 requests per second, but have not requested
// anything for 1 second, you'll immediately be able to request 100.
//
// If taskscaler tells the limiter that too many errors have occurred, no
// scaling can happen until after an exponential backoff delay.
type Limiter struct {
	mu sync.Mutex

	burst   int
	limiter *rate.Limiter

	startedFailingAt time.Time

	delay    time.Time
	newDelay bool
	now      func() time.Time

	random *rand.Rand

	// backoff
	failures int
	minimum  time.Duration
	maximum  time.Duration
	factor   float64
	jitter   float64
}

// New returns a new rate limiter.
func New(limit, burst int) *Limiter {
	lim := rate.Inf
	if limit > 0 {
		lim = rate.Limit(limit)
	}

	if burst <= 0 {
		if lim == rate.Inf {
			burst = 0
		} else {
			burst = limit
		}
	}

	return &Limiter{
		burst:   burst,
		limiter: rate.NewLimiter(lim, burst),
		now:     time.Now,
		random:  rand.New(rand.NewSource(time.Now().UnixNano())),

		minimum: DefaultBackoffMinimum,
		maximum: DefaultBackoffMaximum,
		factor:  DefaultBackoffFactor,
		jitter:  DefaultBackoffJitter,
	}
}

// SetBackoff sets a custom backoff, factor and jitter.
func (l *Limiter) SetBackoff(minimum, maximum time.Duration, factor, jitter float64) {
	l.minimum = minimum
	l.maximum = maximum
	l.factor = factor
	l.jitter = jitter
}

func (l *Limiter) test(now func() time.Time) {
	l.now = now
	l.random = rand.New(rand.NewSource(0))
}

// LimitBurst returns the limit and burst settings.
func (l *Limiter) LimitBurst() (int, int) {
	limit := l.limiter.Limit()
	if limit == rate.Inf {
		return -1, l.burst
	}

	return int(limit), l.burst
}

// Delay returns the current delay and whether the delay has been adjusted since
// the last call.
func (l *Limiter) Delay() (time.Duration, bool) {
	l.mu.Lock()
	defer l.mu.Unlock()

	newDelay := l.newDelay
	l.newDelay = false

	delay := l.delay.Sub(l.now())
	if delay <= 0 {
		return 0, false
	}

	return delay, newDelay
}

// N returns rate limited n, this can be 0 if there's no bucket capacity or
// if we're delayed due to exponential backoff.
func (l *Limiter) N(n int) int {
	l.mu.Lock()
	defer l.mu.Unlock()

	now := l.now()

	if !now.After(l.delay) {
		return 0
	}

	// if we're no longer being delayed, set back the original burst
	if !l.delay.IsZero() {
		l.limiter.SetBurstAt(now, l.burst)
		l.delay = time.Time{}
	}

	take := min(n, int(l.limiter.TokensAt(now)))
	if l.limiter.AllowN(now, take) {
		return take
	}

	return 0
}

// Failure registers a failure. Each failure exponentially increases the
// delay before N returns >0.
func (l *Limiter) Failure() {
	l.mu.Lock()
	defer l.mu.Unlock()

	l.newDelay = true

	if l.failures == 0 {
		now := l.now()

		// on failure, we adjust the burst to match our limit
		// we do this so that after the backoff we can't immediately burst
		l.limiter.SetBurstAt(now, int(l.limiter.Limit()))

		l.startedFailingAt = now
	}

	backoff := math.Pow(float64(l.factor), float64(l.failures)) * float64(l.minimum)
	if backoff > float64(l.maximum) {
		backoff = float64(l.maximum)
	}

	l.delay = l.startedFailingAt.Add(time.Duration(backoff + l.jitter*(l.random.Float64()*float64(l.minimum)*2)))

	l.failures++
}

// Success registers a success. This only resets the failure count if the
// failure delay has been exceeded.
func (l *Limiter) Success() {
	l.mu.Lock()
	defer l.mu.Unlock()

	// we only mark successful if we're no longer being delayed, this helps
	// where a batch request might have a mix of fails/successes. If there
	// was a failure in the batch, we don't let a success nullify that.
	if l.now().After(l.delay) {
		l.failures = 0
	}
}
