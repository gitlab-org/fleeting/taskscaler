package internal

import (
	"context"
	"errors"
	"fmt"
	"sync"
	"time"

	"github.com/hashicorp/go-hclog"
	"resenje.org/singleflight"

	"gitlab.com/gitlab-org/fleeting/fleeting"
	"gitlab.com/gitlab-org/fleeting/fleeting/provider"
)

type Instance struct {
	instance fleeting.Instance
	logger   hclog.Logger
	store    Store

	mu      sync.Mutex
	state   State
	pending int

	preparing bool
	ready     bool

	flight      *singleflight.Group[string, any]
	heartbeatFn func(context.Context, provider.ConnectInfo) error

	groupCtx *CancellableGroupContext
}

type Store interface {
	CommitInstanceState(state State) error
}

type State struct {
	// immutable
	ID                  string
	CapacityPerInstance int
	MaxUseCount         int
	FailureThreshold    int

	// mutable
	Info                    provider.ConnectInfo
	AcquiredSlots           map[int]string
	UsedCount               int
	ConsecutiveFailureCount int
	Removing                bool
	LastUsedAt              time.Time
}

type SaveStateAction string

const (
	ActionAcquire            = SaveStateAction("acquire")
	ActionRelinquish         = SaveStateAction("relinquish")
	ActionRefreshConnectInfo = SaveStateAction("refresh connect info")
	ActionRemoveIfExpired    = SaveStateAction("remove if expired")
	ActionRemove             = SaveStateAction("remove")
	ActionHealthSuccess      = SaveStateAction("health success")
	ActionHealthFailure      = SaveStateAction("health failure")
)

var (
	ErrInstanceReachedMaxUseCount       = errors.New("instance reached max use count")
	ErrInstanceUnhealthy                = errors.New("instance is unhealthy")
	ErrInstanceExceededMaxIdleTime      = errors.New("instance exceeded max idle time")
	ErrInstancePreparationFailed        = errors.New("instance connection preparation failed")
	ErrInstanceReadyUpPreparationFailed = errors.New("instance ready up preparation failed")
	ErrInstanceExternallyRemoved        = errors.New("instance externally removed")
)

func New(instance fleeting.Instance, logger hclog.Logger, state State, store Store, flight *singleflight.Group[string, any], heartbeatFn func(context.Context, provider.ConnectInfo) error) *Instance {
	logger = logger.With("instance", instance.ID())

	if state.AcquiredSlots == nil {
		state.AcquiredSlots = map[int]string{}
	}

	return &Instance{
		instance:    instance,
		logger:      logger,
		store:       store,
		state:       state,
		flight:      flight,
		heartbeatFn: heartbeatFn,
		groupCtx:    NewCancellableGroupContext(),
	}
}

func (inst *Instance) ID() string {
	return inst.instance.ID()
}

func (inst *Instance) Acquire(ctx context.Context, key string) (slot int, success bool) {
	if err, ok := inst.Heartbeat(ctx); !ok {
		if err != nil {
			inst.logger.Error("acquire heartbeat failure", "err", err)
		}
		return 0, false
	}

	inst.mu.Lock()
	defer inst.mu.Unlock()

	// check to see if we can still acquire, as things may have changed since
	// before the heartbeat (such as instance being removed).
	if !inst.canAcquire(false) {
		return 0, false
	}

	inst.state.UsedCount++
	for i := 0; i < inst.state.CapacityPerInstance; i++ {
		if _, occupied := inst.state.AcquiredSlots[i]; !occupied {
			inst.state.AcquiredSlots[i] = key

			if !inst.saveState(ActionAcquire) {
				// rollback state changes if unable to save
				inst.state.UsedCount--
				delete(inst.state.AcquiredSlots, i)
				return 0, false
			}

			return i, true
		}
	}

	panic("no remaining slots")
}

func (inst *Instance) Heartbeat(ctx context.Context) (error, bool) {
	if inst.heartbeatFn == nil {
		return nil, true
	}

	inst.mu.Lock()
	ok := inst.canAcquire(true)
	if ok {
		// temporarily increase pending so that other callers to this method
		// will fail the checks above, effectively simulating that the instance
		// has been acquired by something else already.
		inst.pending++
	}
	inst.mu.Unlock()

	if !ok {
		return nil, false
	}

	defer func() {
		inst.mu.Lock()
		inst.pending--
		inst.mu.Unlock()
	}()

	ctx, cancel := inst.WithContext(ctx)
	defer cancel()

	_, _, err := inst.flight.Do(ctx, inst.ID(), func(ctx context.Context) (any, error) {
		inst.logger.Trace("acquire heartbeat started")

		start := time.Now()
		defer func() {
			inst.logger.Trace("acquire heartbeat finished", "took", time.Since(start))
		}()

		info, err := inst.ConnectInfo(ctx)
		if err != nil {
			return nil, fmt.Errorf("connect info: %w", err)
		}

		return nil, inst.heartbeatFn(ctx, info)
	})

	return err, err == nil
}

func (inst *Instance) canAcquire(includePending bool) bool {
	switch {
	case inst.state.Removing, !inst.ready:
		return false

	case inst.reachedCapacityPerInstance(includePending):
		return false

	case inst.reachedMaxUseCount(includePending):
		return false
	}

	return true
}

func (inst *Instance) reachedMaxUseCount(includePending bool) bool {
	useCount := inst.state.UsedCount
	if includePending {
		useCount += int(inst.pending)
	}

	return inst.state.MaxUseCount > 0 && useCount >= inst.state.MaxUseCount
}

func (inst *Instance) reachedCapacityPerInstance(includePending bool) bool {
	acquiredSlots := len(inst.state.AcquiredSlots)
	if includePending {
		acquiredSlots += int(inst.pending)
	}

	return acquiredSlots >= inst.state.CapacityPerInstance
}

func (inst *Instance) unhealthy() bool {
	return inst.state.ConsecutiveFailureCount >= inst.state.FailureThreshold
}

func (inst *Instance) Relinquish(slot int) {
	inst.mu.Lock()
	defer inst.mu.Unlock()
	if _, ok := inst.state.AcquiredSlots[slot]; ok {
		delete(inst.state.AcquiredSlots, slot)
	} else {
		panic("relinquish of slot not acquired")
	}

	inst.state.LastUsedAt = time.Now()
	if inst.reachedMaxUseCount(false) && len(inst.state.AcquiredSlots) == 0 {
		inst.remove(ErrInstanceReachedMaxUseCount)
	}

	inst.saveState(ActionRelinquish)
}

func (inst *Instance) WithContext(ctx context.Context) (context.Context, context.CancelFunc) {
	return inst.groupCtx.WithContext(ctx)
}

// Capacity returns the acquired and unavailable capacity of an instance.
//
// The unavailable capacity is typically 0, indicating that all capacity is
// available (other than whatever the acquired capacity is).
//
// The unavailable capacity is above zero when a max used count is configured,
// and the used count is affecting the capacity:
//   - if you have a capacity of 5 and a used count of 1/100, you still have a
//     capacity of 5. The unavailable capacity is therefore 0.
//   - if you have a capacity of 5 and a used count of 96/100, you now only
//     have a capacity of 4. The unavailable capacity is therefore 1.
func (inst *Instance) Capacity() (acquired, unavailable int, deleted bool) {
	inst.mu.Lock()
	defer inst.mu.Unlock()

	if !inst.instance.DeletedAt().IsZero() {
		return 0, 0, true
	}

	acquired = len(inst.state.AcquiredSlots)

	if inst.state.Removing {
		return acquired, inst.state.CapacityPerInstance - acquired, false
	}

	if inst.state.MaxUseCount <= 0 {
		return acquired, 0, false
	}

	if !inst.state.Removing && inst.state.MaxUseCount > 0 {
		// used and acquired are incremented together, and we don't want
		// the unavailable count to include the current acquired count,
		// so we deduct it here.
		remainingUses := inst.state.MaxUseCount - (inst.state.UsedCount - acquired)
		if remainingUses < inst.state.CapacityPerInstance {
			unavailable = inst.state.CapacityPerInstance - remainingUses
		}
	}

	return acquired, unavailable, false
}

func (inst *Instance) IsReady() bool {
	inst.mu.Lock()
	defer inst.mu.Unlock()

	return inst.ready
}

func (inst *Instance) Run(fn func(info provider.ConnectInfo) error) error {
	return fn(inst.state.Info)
}

// ConnectInfo returns the connection information for the instance.
//
// If the connection info has expired, it will be attempted to be refreshed. If
// that attempt fails, the error will be returned along with the expired
// connection info. On success, the new connection info will be cached for
// subsequent calls. If HealthFailure() is called, the connection cache will
// be invalidated.
func (inst *Instance) ConnectInfo(ctx context.Context) (provider.ConnectInfo, error) {
	inst.mu.Lock()
	defer inst.mu.Unlock()

	// the connection cache has expired if expiration is set and we're 10 seconds away
	// from expiration
	expired := inst.state.Info.Expires != nil && inst.state.Info.Expires.Before(time.Now().Add(10*time.Second))

	// refresh if connection has errors or cache has expired
	if inst.state.ConsecutiveFailureCount > 0 || expired || inst.state.Info.ID == "" {
		info, err := inst.instance.ConnectInfo(ctx)
		if err != nil {
			return inst.state.Info, fmt.Errorf("refreshing connect info: %w", err)
		}
		inst.state.Info = info

		inst.saveState(ActionRefreshConnectInfo)
	}

	return inst.state.Info, nil
}

type UpFunc func(id string, info provider.ConnectInfo, cause fleeting.Cause) error

type readyFunc func(time.Duration, error)

func (inst *Instance) Prepare(ctx context.Context, cause fleeting.Cause, upFunc UpFunc, readyFn readyFunc) {
	inst.mu.Lock()
	defer inst.mu.Unlock()

	if inst.preparing || inst.ready {
		return
	}

	inst.preparing = true
	start := time.Now()
	ready := func(msg string, err error) {
		preparationTime := time.Since(start)
		if err == nil {
			inst.logger.Info(msg, "took", preparationTime)
		} else {
			inst.logger.Error(msg, "took", preparationTime, "err", err)
		}

		if readyFn != nil {
			readyFn(preparationTime, err)
		}
	}

	go func() {
		defer func() {
			inst.mu.Lock()
			inst.preparing = false
			inst.mu.Unlock()
		}()

		if inst.state.Removing {
			var errInstanceRemoval = errors.New("instance marked for removal")
			ready("ready up failed because instance is marked for removal", errInstanceRemoval)
			inst.instance.Delete()
			return
		}

		ctx, cancel := inst.WithContext(ctx)
		defer cancel()

		if !inst.prepareWait(ctx) {
			return
		}

		info, err := inst.ConnectInfo(ctx)
		if err != nil {
			ready("connection preparation failed", err)
			inst.Remove(ErrInstancePreparationFailed)
			return
		}
		inst.state.Info = info

		if upFunc != nil {
			if err := upFunc(inst.ID(), info, cause); err != nil {
				ready("ready up preparation failed", err)
				inst.Remove(ErrInstanceReadyUpPreparationFailed)
				return
			}
		}

		inst.mu.Lock()
		inst.ready = true
		inst.mu.Unlock()

		ready("instance is ready", nil)
	}()
}

func (inst *Instance) prepareWait(ctx context.Context) bool {
	ctx, cancel := context.WithTimeout(ctx, time.Minute)
	defer cancel()

	return inst.instance.ReadyWait(ctx)
}

func (inst *Instance) Unready() {
	inst.mu.Lock()
	defer inst.mu.Unlock()

	if !inst.ready {
		return
	}

	inst.ready = false
	inst.state.ConsecutiveFailureCount = 0
}

// RemoveIfExpired determines if an instances should be deleted according to
// several rules and then deletes the instance.
func (inst *Instance) RemoveIfExpired(minIdleTime time.Duration) (removed bool) {
	inst.mu.Lock()
	defer inst.mu.Unlock()

	defer func() {
		if removed {
			inst.saveState(ActionRemoveIfExpired)
		}
	}()

	// don't expire acquired instances
	if len(inst.state.AcquiredSlots) > 0 {
		return false
	}

	if inst.reachedMaxUseCount(false) {
		inst.remove(ErrInstanceReachedMaxUseCount)
		return true
	}

	if inst.unhealthy() {
		inst.remove(ErrInstanceUnhealthy)
		return true
	}

	// use the last idle time if available, otherwise,
	// the time the instance was provisioned at
	idleTime := inst.instance.ProvisionedAt()
	if inst.state.LastUsedAt.After(idleTime) {
		idleTime = inst.state.LastUsedAt
	}

	// keep the instance if it hasn't yet exceeded the minimum idle time
	if time.Since(idleTime) < minIdleTime {
		return false
	}

	inst.remove(ErrInstanceExceededMaxIdleTime)

	return true
}

// Remove marks an instance for removal
func (inst *Instance) Remove(reason error) {
	inst.mu.Lock()
	defer inst.mu.Unlock()
	inst.remove(reason)

	inst.saveState(ActionRemove)
}

func (inst *Instance) remove(reason error) {
	if inst.state.Removing {
		return
	}

	inst.logger.Info("instance marked for removal", "reason", reason)

	inst.instance.Delete()
	inst.state.Removing = true
	inst.groupCtx.Cancel(reason)
}

// RemovedExternally is used to signal that this instance no longer exists
// according to the instance group.
func (inst *Instance) RemovedExternally() {
	inst.groupCtx.Cancel(ErrInstanceExternallyRemoved)

	inst.mu.Lock()
	defer inst.mu.Unlock()

	if inst.state.Removing {
		return
	}

	inst.logger.Error("instance unexpectedly removed", "slots", inst.state.AcquiredSlots, "used", inst.state.UsedCount, "max-use-count", inst.state.MaxUseCount)
}

// HealthSuccess reports a successful connection verifying health of
// the instance
func (inst *Instance) HealthSuccess() {
	inst.mu.Lock()
	defer inst.mu.Unlock()

	inst.logger.Info("health success", "failure-count", inst.state.ConsecutiveFailureCount)

	inst.state.ConsecutiveFailureCount = 0

	inst.saveState(ActionHealthSuccess)
}

// HealthFailure reports an unsuccessful connection verifying the
// unhealthiness of the instance
func (inst *Instance) HealthFailure() {
	inst.mu.Lock()
	defer inst.mu.Unlock()
	inst.state.ConsecutiveFailureCount++

	inst.logger.Warn("health failure", "failure-count", inst.state.ConsecutiveFailureCount)

	inst.saveState(ActionHealthFailure)
}

func (inst *Instance) saveState(action SaveStateAction) bool {
	if inst.store == nil {
		return true
	}

	if err := inst.store.CommitInstanceState(inst.state); err != nil {
		inst.logger.Error("could not commit instance state", "action", string(action), "err", err)
		return false
	}

	return true
}
