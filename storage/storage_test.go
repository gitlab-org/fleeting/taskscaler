package storage

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestCommit(t *testing.T) {
	store, err := NewFileStorage(t.TempDir())
	require.NoError(t, err)

	require.NoError(t, store.CommitInstanceState(InstanceState{
		ID:          "1234",
		UsedCount:   123,
		MaxUseCount: 4000,
	}))

	state, err := store.Load()
	require.NoError(t, err)
	require.Equal(t, 1, len(state.Instances))

	assert.Equal(t, "1234", state.Instances[0].ID)
	assert.Equal(t, 123, state.Instances[0].UsedCount)
	assert.Equal(t, 4000, state.Instances[0].MaxUseCount)
}

func TestDelete(t *testing.T) {
	store, err := NewFileStorage(t.TempDir())
	require.NoError(t, err)

	require.NoError(t, store.CommitInstanceState(InstanceState{
		ID:          "1234",
		UsedCount:   123,
		MaxUseCount: 4000,
	}))

	require.NoError(t, store.CommitInstanceState(InstanceState{
		ID:          "6789",
		UsedCount:   2,
		MaxUseCount: 5,
	}))

	state, err := store.Load()
	require.NoError(t, err)
	require.Equal(t, 2, len(state.Instances))

	require.NoError(t, store.DeleteInstanceState("1234"))

	state, err = store.Load()
	require.NoError(t, err)
	require.Equal(t, 1, len(state.Instances))

	assert.Equal(t, "6789", state.Instances[0].ID)
	assert.Equal(t, 2, state.Instances[0].UsedCount)
	assert.Equal(t, 5, state.Instances[0].MaxUseCount)
}
