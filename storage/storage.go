package storage

import (
	"crypto/sha256"
	"encoding/hex"
	"encoding/json"
	"errors"
	"fmt"
	"os"
	"path/filepath"

	"gitlab.com/gitlab-org/fleeting/taskscaler/internal"
)

var ErrNoStateFound = errors.New("no state found")

type State struct {
	Instances []InstanceState
}

type AcquisitionState struct {
	InstanceID string
	Slot       int
}

type InstanceState = internal.State

type Storage interface {
	Load() (State, error)

	CommitInstanceState(state InstanceState) error
	DeleteInstanceState(instance string) error
}

type FileStorage struct {
	dir string
}

func NewFileStorage(dir string) (*FileStorage, error) {
	if err := os.MkdirAll(filepath.Join(dir, "instances"), 0o700); err != nil {
		return nil, err
	}

	return &FileStorage{dir: dir}, nil
}

func (fs *FileStorage) Load() (State, error) {
	var state State

	dir := filepath.Join(fs.dir, "instances")
	instances, err := os.ReadDir(dir)
	if err != nil {
		return state, fmt.Errorf("read dir: %w", err)
	}

	for _, instanceFile := range instances {
		data, err := os.ReadFile(filepath.Join(dir, instanceFile.Name()))
		if err != nil {
			return state, fmt.Errorf("reading instance state file %v: %w", instanceFile.Name(), err)
		}

		var instance InstanceState
		if err := json.Unmarshal(data, &instance); err != nil {
			return state, fmt.Errorf("json unmarshal %v: %w", instanceFile.Name(), err)
		}

		state.Instances = append(state.Instances, instance)
	}

	return state, nil
}

func (fs *FileStorage) CommitInstanceState(state InstanceState) error {
	return save(fs.dir, instancePath(state.ID), state)
}

func (fs *FileStorage) DeleteInstanceState(instance string) error {
	return os.RemoveAll(filepath.Join(fs.dir, instancePath(instance)))
}

func save(dir, dest string, state any) error {
	f, err := os.CreateTemp(dir, "state-temp-*")
	if err != nil {
		return fmt.Errorf("creating temporary state file: %w", err)
	}
	defer os.RemoveAll(f.Name())
	defer f.Close()

	if err := json.NewEncoder(f).Encode(state); err != nil {
		return fmt.Errorf("json marshal: %w", err)
	}

	if err := f.Close(); err != nil {
		return fmt.Errorf("creating state file: %w", err)
	}

	return os.Rename(f.Name(), filepath.Join(dir, dest))
}

func instancePath(p string) string {
	key := sha256.Sum256([]byte(p))

	return filepath.Join("instances", hex.EncodeToString(key[:]))
}
